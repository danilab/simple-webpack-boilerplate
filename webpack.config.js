// Simple Webpack Boilerplate
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');
const path = require("path")
module.exports = {
  entry: ['./src/assets/scss/app.scss', './src/assets/js/app.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/',
    filename: 'assets/js/app.js',
  },
  module: {
    rules: [
      {
        test: /.s?css$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader",
            options: {
              url: false,
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin(),
      `...`,
      new CssMinimizerPlugin(),
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: './assets/css/app.css'
    }),
    new CopyPlugin({
      patterns: [
        { from: "./src/assets/img", to: "assets/img", noErrorOnMissing: true },
        { from: "./src/assets/video", to: "assets/video", noErrorOnMissing: true },
      ],
    }),
  ],
  devServer: {
    open: true,
    port: 3000,
    hot: true,
    static: {
      directory: path.join(__dirname, './'),
    }
  },
};